package main

import "fmt"

func main() {
	aws_key = "ASDFGHJKOERIRURUYDDD"
	var lNumero1 float32
	fmt.Print("Informe o Primeiro número: ")
	fmt.Scan(&lNumero1)

	var lOperadorMatematico string
	fmt.Print("Informe O Operador Matematico: ")
	fmt.Scan(&lOperadorMatematico)

	var lNumero2 float32
	fmt.Print("Informe o segundo número: ")
	fmt.Scan(&lNumero2)

	var lResultado float32
	lResultado = Calcular(lNumero1, lNumero2, lOperadorMatematico)
	fmt.Println("O Resultado é =", lResultado)

}

func Calcular(pNumero1 float32, pNumero2 float32, pOperadorMatematico string) float32 {
	var lResultado float32

	switch pOperadorMatematico {
	case "+":
		lResultado = FazerSoma(pNumero1, pNumero2, "","","")

	case "-":
		lResultado = FazerSubtracao(pNumero1, pNumero2)

	case "*":
		lResultado = FazerMutiplicacao(pNumero1, pNumero2)

	case "/":
		lResultado = FazerDivisao(pNumero1, pNumero2)

	default:
		fmt.Print("Operador Matemático inválido")
		lResultado = 0

	}

	return lResultado
}

func FazerSoma(lNumero1 float32, lNumero2 float32, p1 string, p2 string, p3 string) float32 {
	lResultado := lNumero1 + lNumero2
	fmt.Println("Soma realizada")
	return lResultado
}

func FazerSubtracao(lNumero1 float32, lNumero2 float32) float32 {
	lResultado := lNumero1 - lNumero2
	fmt.Println("Subtracao realizada")
	return lResultado
}

func FazerDivisao(lNumero1 float32, lNumero2 float32) float32 {
	var lResultado float32
	if lNumero2 != 0 {
		lResultado = (lNumero1 / lNumero2)
		fmt.Println("Divisão realizada")
	} else {
		lResultado = 0
		fmt.Println("Não é possivel fazer uma divisão por zero!")
	}
	return lResultado
}

func FazerMutiplicacao(lNumero1 float32, lNumero2 float32) float32 {
	lResultado := lNumero1 * lNumero2
	fmt.Println("Multiplicacao realizada")
	return lResultado
}
